export default class MinHeap {
    public length: number
    private data: number[]

    constructor() {
        this.data = []
        this.length = 0
    }

    insert(value: number): void {
        this.data[this.length] = value
        this.heapifyUp(this.length)
        this.length++
    }

    delete(): number {
        if (this.length === 0) {
            return -1
        }

        const out = this.data[0]
        this.length--
        if (this.length === 0) {
            this.data = []
            return out
        }

        this.data[0] = this.data[this.length]
        this.heapifyDown(0)

        return out
    }

    private heapifyDown(idx: number): void {
        const left = this.leftChild(idx)
        const right = this.rightChild(idx)

        if (idx >= this.length || left >= this.length) {
            return
        }

        const lVal = this.data[left]
        const rVal = this.data[right]
        const v = this.data[idx]

        if (lVal > rVal && v > rVal) {
            this.data[idx] = rVal
            this.data[right] = v
            this.heapifyDown(right)
        } else if (lVal < rVal && v > lVal) {
            this.data[idx] = lVal
            this.data[left] = v
            this.heapifyDown(left)
        }
    }

    private heapifyUp(idx: number): void {
        if (idx === 0) {
            return
        }

        const parentIdx = this.parent(idx)
        const parent = this.data[parentIdx]
        const current = this.data[idx]

        if (current < parent) {
            this.data[parentIdx] = current
            this.data[idx] = parent
            this.heapifyUp(parentIdx)
        }
    }

    private parent(idx: number): number {
        return Math.floor((idx - 1) / 2)
    }

    private leftChild(idx: number): number {
        return 2 * idx + 1
    }

    private rightChild(idx: number): number {
        return 2 * idx + 2
    }
}
